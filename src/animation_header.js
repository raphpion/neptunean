//* animation_header.js
// Modifier l'opacité des éléments du header en décalé
// © Copyright Raphaël Pion 2020 ─ tous droits réservés

const logo = document.getElementById('logo')
const header_h2 = document.querySelector('header h2')
const logoText = document.getElementById('logo-text')

logo.style.opacity = 1

setTimeout(function () {
  logo.style.opacity = 0.2
  logoText.style.opacity = 1
  setTimeout(function () {
    header_h2.style.opacity = 1
  }, 600)
}, 2000)

//* onscroll.js
// Gestion des événements lorsque le client scroll sur la page
// © Copyright Raphaël Pion 2020 - tous droits réservés

const nav = document.querySelector('nav')
const navLinks = document.querySelectorAll('nav a')
const sectionOffsets = []

stickyNav()
window.onscroll = scrollHandler
window.onresize = scrollHandler

function getSectionYOffsets() {
  sectionOffsets[0] = document.getElementById('accueil').offsetTop
  sectionOffsets[1] = document.getElementById('nouveautes').offsetTop
  sectionOffsets[2] = document.getElementById('marchandise').offsetTop
  sectionOffsets[3] = document.getElementById('blogue').offsetTop
  sectionOffsets[4] = document.getElementById('contact').offsetTop
}

function getViewportHeight() {
  return window.innerHeight
}

function scrollHandler() {
  stickyNav()
  scrollSpy()
}

function scrollSpy() {
  for (let link of navLinks) link.classList.remove('active')
  getSectionYOffsets()
  if (window.pageYOffset >= sectionOffsets[4]) {
    navLinks[4].classList.add('active')
    document.title = 'Contact | Neptunean'
  } else if (window.pageYOffset >= sectionOffsets[3]) {
    navLinks[3].classList.add('active')
    document.title = 'Blogue | Neptunean'
  } else if (window.pageYOffset >= sectionOffsets[2]) {
    navLinks[1].classList.add('active')
    document.title = 'Marchandise | Neptunean'
  } else if (window.pageYOffset >= sectionOffsets[1]) {
    navLinks[0].classList.add('active')
    document.title = 'Nouveautés | Neptunean'
  } else {
    navLinks[2].classList.add('active')
    document.title = 'Neptunean'
  }
}

function stickyNav() {
  let navOffset = getViewportHeight() - 60
  if (nav.classList.contains('fixed')) {
    if (window.pageYOffset < navOffset) nav.classList.remove('fixed')
  } else if (window.pageYOffset >= navOffset) nav.classList.add('fixed')
}

//* textarea.js
// La fonction textAreaAdjust ajuste la hauteur d'un text area en paramètre selon le contenu
// © Copyright Raphaël Pion 2020 ─ tous droits réservés

function textAreaAdjust(el) {
  el.style.height = '1px'
  el.style.height = 15 + el.scrollHeight + 'px'
}

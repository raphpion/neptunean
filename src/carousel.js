//* carousel.js
// Lorsque le client appuie sur les contrôles du carousel, on change la slide active
// © Copyright Raphaël Pion 2020 ─ tous droits réservés

const carouselControl1 = document.getElementById('carousel-control-1')
const carouselControl2 = document.getElementById('carousel-control-2')
const carouselControl3 = document.getElementById('carousel-control-3')

carouselControl1.onclick = () => {
  carouselHandler(1)
}
carouselControl2.onclick = () => {
  carouselHandler(2)
}
carouselControl3.onclick = () => {
  carouselHandler(3)
}

function carouselHandler(n) {
  let carousel = document.getElementById('carousel')
  n == 1 ? carouselControl1.classList.add('active') : carouselControl1.classList.remove('active')
  n == 2 ? carouselControl2.classList.add('active') : carouselControl2.classList.remove('active')
  n == 3 ? carouselControl3.classList.add('active') : carouselControl3.classList.remove('active')
  if (n == 1) carousel.classList.remove('slide-2', 'slide-3')
  n == 2 ? carousel.classList.add('slide-2') : carousel.classList.remove('slide-2')
  n == 3 ? carousel.classList.add('slide-3') : carousel.classList.remove('slide-3')
}

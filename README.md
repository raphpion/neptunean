# Neptunean
Projet final du cours 1W1 - Mise en page Web
Techniques d'intégration multimédia, collège Maisonneuve

## Développement
Le projet présentait beaucoup de contraintes qui ont joué sur le résultat final de la page. Dans le futur, je referais ce projet de manière à ce que le site s'adapte mieux à la résolution de l'appareil et je permettrais aux sections de prendre plus que 100% du viewport pour aérer un peu le contenu.

## Ressources
Je ne possède aucun droit sur les images de la catégorie *marchandise*.
Logos, musique et développement par [Raphaël Pion](https://www.raphaelpion.com/)